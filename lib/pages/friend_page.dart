import 'package:flutter/material.dart';
import 'package:sosmed_app/constant.dart';

class FriendPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: blackColor),
        backgroundColor: whiteColor,
        automaticallyImplyLeading: true,
        title: Text(
          "Arif Satria",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Center(
        child: Flexible(
          child: Hero(
            tag: 'Friends Pic',
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 12),
              child: Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    CircleAvatar(
                      radius: 80,
                      backgroundImage:
                          AssetImage("assets/images/friend_pic1.png"),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Arif Satria",
                      style: TextStyle(
                          fontSize: 24,
                          color: blackColor,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              "56",
                              style: TextStyle(fontSize: 24, color: blueColor),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Posts",
                              style: TextStyle(fontSize: 16),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              "1M",
                              style: TextStyle(fontSize: 24, color: blueColor),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Followers",
                              style: TextStyle(fontSize: 16),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                              "220",
                              style: TextStyle(fontSize: 24, color: blueColor),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Text(
                              "Following",
                              style: TextStyle(fontSize: 16),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    Divider(
                      height: 2,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    MaterialButton(
                      onPressed: () {},
                      color: blueColor,
                      minWidth: double.infinity,
                      padding: EdgeInsets.symmetric(vertical: 14),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      child: Text(
                        "FOLLOW",
                        style: TextStyle(fontSize: 14, color: whiteColor),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
