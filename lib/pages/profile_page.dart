import 'package:flutter/material.dart';
import 'package:sosmed_app/constant.dart';
import 'package:sosmed_app/pages/friend_page.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/profile_bg.png"),
                  alignment: Alignment.topCenter,
                  fit: BoxFit.fitWidth)),
          child: Column(
            children: [
              SizedBox(
                height: 260,
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40), color: whiteColor),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: 48,
                                backgroundImage:
                                    AssetImage("assets/images/profile_pic.png"),
                              ),
                              SizedBox(
                                width: 28,
                              ),
                              Expanded(
                                  child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "Budi Doremi",
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: blueColor,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text("Indonesia"),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  MaterialButton(
                                    onPressed: () {},
                                    color: blueColor,
                                    minWidth: double.infinity,
                                    padding: EdgeInsets.symmetric(vertical: 14),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(4)),
                                    child: Text(
                                      "FOLLOW",
                                      style: TextStyle(
                                          fontSize: 14, color: whiteColor),
                                    ),
                                  )
                                ],
                              ))
                            ],
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                children: [
                                  Text(
                                    "56",
                                    style: TextStyle(
                                        fontSize: 24, color: blueColor),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    "Posts",
                                    style: TextStyle(fontSize: 16),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Text(
                                    "1M",
                                    style: TextStyle(
                                        fontSize: 24, color: blueColor),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    "Followers",
                                    style: TextStyle(fontSize: 16),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Text(
                                    "220",
                                    style: TextStyle(
                                        fontSize: 24, color: blueColor),
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    "Following",
                                    style: TextStyle(fontSize: 16),
                                  )
                                ],
                              )
                            ],
                          ),
                          SizedBox(
                            height: 14,
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            "Friends",
                            style: TextStyle(fontSize: 20, color: blueColor),
                          )
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 28,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return FriendPage();
                              }));
                            },
                            child: Hero(
                              tag: 'Friends Pic',
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                child: CircleAvatar(
                                  radius: 20,
                                  backgroundImage: AssetImage(
                                      "assets/images/friend_pic1.png"),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic2.png"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic3.png"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic4.png"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic5.png"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic6.png"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: CircleAvatar(
                              radius: 20,
                              backgroundImage:
                                  AssetImage("assets/images/friend_pic1.png"),
                            ),
                          ),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 32, vertical: 12),
                      child: Text(
                        "Photos",
                        style: TextStyle(color: blueColor, fontSize: 20),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 28,
                          ),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(
                                  "assets/images/photo1.png",
                                  height: 200,
                                  fit: BoxFit.fitHeight,
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(
                                  "assets/images/photo2.png",
                                  height: 200,
                                  fit: BoxFit.fitHeight,
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.asset(
                                  "assets/images/photo3.png",
                                  height: 200,
                                  fit: BoxFit.fitHeight,
                                ),
                              )),
                          SizedBox(
                            width: 28,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
