// @dart=2.9
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:sosmed_app/constant.dart';
import 'package:sosmed_app/pages/profile_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    indicator() {
      return Container(
        width: 8,
        height: 8,
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(color: whiteColor, shape: BoxShape.circle),
      );
    }

    activeIndicator() {
      return Container(
        width: 14,
        height: 14,
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
            shape: BoxShape.circle, border: Border.all(color: whiteColor)),
        child: Center(
          child: Container(
            width: 8,
            height: 8,
            decoration:
                BoxDecoration(color: whiteColor, shape: BoxShape.circle),
          ),
        ),
      );
    }

    return Scaffold(
      body: Stack(
        children: [
          CarouselSlider(
              items: [0, 1, 2, 3].map((e) {
                return Image.asset("assets/images/onboarding.png");
              }).toList(),
              options: CarouselOptions(
                  initialPage: currentIndex,
                  height: double.infinity,
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {
                    setState(() {
                      currentIndex = index;
                    });
                  })),
          Padding(
            padding: EdgeInsets.only(left: 40, top: 80),
            child: Row(
              children: [0, 1, 2, 3].map((item) {
                if (item == currentIndex) return activeIndicator();
                return indicator();
              }).toList(),
            ),
          ),
          Center(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 36),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'Explore Your\nFriend Here',
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 46,
                        fontWeight: FontWeight.w500),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Sign In to meet your friend, join group\nand live your best life.',
                    style: TextStyle(color: whiteColor, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileScreen()));
                    },
                    color: whiteColor,
                    minWidth: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(
                      "Sign Up with email address",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  MaterialButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileScreen()));
                    },
                    color: facebookColor,
                    minWidth: double.infinity,
                    padding: EdgeInsets.symmetric(vertical: 16),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/facebook_logo.png"),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Sign Up with Facebook",
                          style: TextStyle(fontSize: 16, color: whiteColor),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Term & Condition Policies",
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline),
                  ),
                  SizedBox(
                    height: 100,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
